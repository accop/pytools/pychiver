"""
Static definitions of ESS archiver instances
"""

# ARCHIVERS
ARCHIVER_01 = "http://archiver-linac-01.tn.esss.lu.se"
ARCHIVER_WF_01 = "http://archiver-linac-wf-01.tn.esss.lu.se"
DEFAULT_ARCHIVER = ARCHIVER_01

# GIT PROJECT IDS
DEFAULT_ARCHIVER_URL = "https://gitlab.esss.lu.se"
DEFAULT_ARCHIVER_CONF = 8512  # Linac Archiver Appliance Project ID
WAVEFORM_ARCHIVER_CONF = 8513  # Waveforms Archiver Appliance Project ID
LAB_ARCHIVER_CONF = 8536  # Lab Archiver Appliance Project ID
NIN_ARCHIVER_CONF = 6306  # Neutron Instruments Archiver Appliance Project ID

# SAVE RESTORE
SAVE_RESTORE = "https://jmasar.tn.esss.lu.se"
SAVE_RESTORE_TEST = "https://jmasar-lab-02.cslab.esss.lu.se"
DEFAULT_SAVE_RESTORE = SAVE_RESTORE

DEFAULT_MAX_EXTRACTION_SIZE = 5000000  # 5 MB
